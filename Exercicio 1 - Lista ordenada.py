def ordenada(lista):
    listaAux = lista[:]
    listaAux.sort()

    for i in range (len(lista)):
        if(listaAux[i] != lista[i]):
            return False
    return True

def main():
    listOrdenada = [1, 2, 3]
    listNotOrdenada = [3, 5, 1]

    print (ordenada(listOrdenada))
    print(ordenada(listNotOrdenada))


main()