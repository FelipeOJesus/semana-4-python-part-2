def ordena(lista):
    for i in range(len(lista)):
        for j in range (len(lista)):
            if(lista[i] < lista[j]):
                lista[i], lista[j] = lista[j], lista[i]
    return lista

def main():
    lista = [10, 5, 5, 7, 2, 8, 9, 1]
    print (ordena(lista))

main()